# Front end

created using [create-react-app](https://reactjs.org/)

## Steps to preview app

-   clone the repo

```git
git clone https://gitlab.com/intern-project1/client.git
```

-   move to working directory

```shell
cd client
```

-   install dependencies

```shell
yarn install
```

-   start development server

```shell
yarn start
```

## App routes

```

/

/dashboard
/dashboard/view-projects
/dashboard/change-username
/dashboard/change-email
/dashboard/change-password
/dashboard/create-project

/project/{project_id}/{project_slug}/view-discussion
/project/{project_id}/{project_slug}/make-query

/admin

```

## Z-indexes

```
.admin-sidebar : 10
.admin-workspace : 1
.dashboard-sidebar : 10
.dashboard-filter-container : 10
.dashboard-workspace : 1
.wholescreen-loader-container : 1000
```
