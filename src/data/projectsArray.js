import axios from 'axios';
import { getToken } from '../helpers/authHelpers';

export let projectsArr = [];

export const fetchProjects = async () => {
    try {
        const res = await axios.get('http://localhost:8000/api/projects/view-projects', {
            headers: {
                Authorization: getToken(),
            },
        });
        projectsArr = res.data.projects;
    } catch (err) {
        console.log(err);
        console.log(err.response);
    }
};

export const returnStoredProjects = async () => {
    if (!projectsArr.length) {
        await fetchProjects();
    }
    return projectsArr;
};
