export const saveToken = (token) => {
    if (!token) {
        return { success: false, message: 'token not received' };
    }
    localStorage.removeItem('token');
    localStorage.setItem('token', token);
    return {
        success: true,
    };
};

export const checkIfTokenExists = () => {
    const token = localStorage.getItem('token');
    if (token) {
        return true;
    } else {
        return null;
    }
};

export const getToken = () => {
    return 'Bearer ' + localStorage.getItem('token');
};

export const removeToken = () => {
    localStorage.removeItem('token');
    return { success: true };
};
