import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Admin from './pages/Admin';
import Auth from './pages/Auth';
import Dashboard from './pages/Dashboard';
import Project from './pages/Project';
import './App.css';

function App() {
    return (
        <Router>
            <div className="main-div">
                <Switch>
                    <Route exact path="/" component={Auth} />
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/admin" component={Admin} />
                    <Route path="/project/:id/:slug" component={Project} />
                </Switch>
            </div>
        </Router>
    );
}

export default App;
