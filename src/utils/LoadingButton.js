import React from 'react';

function LoadingButton(props) {
    const { className } = props;
    return (
        <button className={className} disabled={true}>
            <div className="spinner-border" style={{ width: 18, height: 18, borderWidth: 2 }} role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </button>
    );
}

export default LoadingButton;
