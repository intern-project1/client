import React, { useState } from 'react';
import AdminSidebar from '../components/Admin/AdminSidebar';
import FilterContainer from '../components/Dashboard/FilterContainer';

import '../styles/admin.css';

function Admin() {
    return (
        <div className="d-flex">
            <AdminSidebar />
            <div className="admin-workspace col-12 col-sm-9 col-lg-10 p-0">
                <FilterContainer />
                <div className="admin-workspace-container p-3">
                    <h1>Admin Workspace</h1>
                </div>
            </div>
        </div>
    );
}

export default Admin;
