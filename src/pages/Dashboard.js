import React, { useState, useEffect } from 'react';
import { Route } from 'react-router-dom';

import FilterContainer from '../components/Dashboard/FilterContainer';
import Sidebar from '../components/Dashboard/Sidebar';
import '../styles/dashboard.css';
import '../styles/dashboardModal.css';
import '../styles/createProjectForm.css';
import WholeScreenLoader from '../utils/WholeScreenLoader';
import CreateProjectForm from '../components/Dashboard/CreateProjectForm';
import ProjectsContainer from '../components/Dashboard/ProjectsContainer';
import { fetchProjects, projectsArr, returnStoredProjects } from '../data/projectsArray';
import { getToken } from '../helpers/authHelpers';
import ChangeEmail from '../components/Modals/ChangeEmail';
import ChangeUsername from '../components/Modals/ChangeUsername';
import ChangePassword from '../components/Modals/ChangePassword';

function Dashboard() {
    const [loader, setLoader] = useState(null);
    const [showSidebar, setShowSidebar] = useState(true);
    const [sidebarCurrentItem, setSidebarCurrentItem] = useState('view_projects');

    const [projects, setProjects] = useState([]);
    const [newProjectCreatedState, setNewProjectCreatedState] = useState(false);

    const fetchProjectsArr = async () => {
        setLoader(true);
        const res = await returnStoredProjects();
        setLoader(false);
        setProjects(res);
    };

    useEffect(() => {
        console.log(getToken());
        fetchProjectsArr();
        return () => {};
    }, []);

    useEffect(() => {
        const asyncFunc = async () => {
            await fetchProjects();
            setProjects(projectsArr);
        };
        asyncFunc();
        return () => {};
    }, [newProjectCreatedState]);

    const toggleSidebarDashboard = () => {
        setShowSidebar((prevState) => !prevState);
    };

    const displayLoader = (bool) => {
        if (bool) {
            setLoader(true);
        } else {
            setLoader(false);
        }
    };

    return (
        <div className="d-flex ">
            {loader && <WholeScreenLoader />}

            <Sidebar
                displayLoader={displayLoader}
                toggleSidebarDashboard={toggleSidebarDashboard}
                showSidebar={showSidebar}
                sidebarCurrentItem={sidebarCurrentItem}
                setSidebarCurrentItem={setSidebarCurrentItem}
            />
            <div className="dashboard-workspace bg-light col-12 col-sm-9 col-lg-10 p-0">
                <FilterContainer toggleSidebarDashboard={toggleSidebarDashboard} showSidebar={showSidebar} />
                <Route path="/dashboard/view-projects" component={() => <ProjectsContainer projects={projects} />} />
                <Route
                    path="/dashboard/create-project"
                    component={() => <CreateProjectForm setNewProjectCreatedState={setNewProjectCreatedState} />}
                />
                <Route path="/dashboard/change-username" component={() => <ChangeUsername />} />
                <Route path="/dashboard/change-email" component={() => <ChangeEmail />} />
                <Route path="/dashboard/change-password" component={() => <ChangePassword />} />
            </div>
        </div>
    );
}

export default Dashboard;
