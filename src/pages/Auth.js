import React, { useState } from 'react';
import CreateUser from '../components/Auth/CreateUser';
import GoToDashboard from '../components/Auth/GoToDashboard';
import '../styles/auth.css';

function Auth() {
    const [createUserClicked, setCreateUserClicked] = useState(false);
    const [goToDashboardClicked, setGoToDashboardClicked] = useState(true);

    return (
        <div className="auth-container">
            <div className="auth-form-container w-50 mx-auto shadow-lg p-4 ">
                <header className="rounded">
                    <span
                        className={`rounded p-2 auth-header-btn ${createUserClicked && 'active'}`}
                        onClick={() => {
                            setCreateUserClicked(true);
                            setGoToDashboardClicked(false);
                        }}
                    >
                        Create member
                    </span>
                    <span
                        className={`rounded p-2 auth-header-btn ${goToDashboardClicked && 'active'}`}
                        onClick={() => {
                            setCreateUserClicked(false);
                            setGoToDashboardClicked(true);
                        }}
                    >
                        {/* Go to dashboard */}
                        Enter dashboard
                    </span>
                </header>
                <main>
                    {createUserClicked && <CreateUser />}
                    {goToDashboardClicked && <GoToDashboard />}
                </main>
            </div>
        </div>
    );
}

export default Auth;
