import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { Route, withRouter } from 'react-router-dom';

import '../styles/project.css';
import WholeScreenLoader from '../utils/WholeScreenLoader';
import { getToken } from '../helpers/authHelpers';
import MakeQueryForm from '../components/Project/MakeQueryForm';
import FilterSection from '../components/Project/FilterSection';
import TitleSection from '../components/Project/TitleSection';

import EverythingBelowNavbar from '../components/Project/EverythingBelowNavbar';

function Project(props) {
    const { id, slug } = props.match.params;

    const [loader, setLoader] = useState(null);
    const [showFilter, setShowFilter] = useState(null);

    const [project, setProject] = useState(null);

    useEffect(() => {
        setLoader(true);
        fetchProjectDiscussion();
        props.history.push(`/project/${id}/${slug}/view-discussion`);
        return () => {};
    }, [id]);

    const projectTitleClicked = () => {
        window.location.reload();
    };

    const fetchProjectDiscussion = () => {
        Axios.get('http://localhost:8000/api/projects/view-discussion/' + id, {
            headers: {
                Authorization: getToken(),
            },
        })
            .then(async (res) => {
                console.log(res);
                setLoader(null);
                if (res.data.success) {
                    setProject(res.data.project);
                }
            })
            .catch((err) => {
                console.log(err);
                console.log(err.response);
                setLoader(null);
            });
    };

    return (
        <>
            {loader && <WholeScreenLoader />}
            <div className="d-flex" style={{ position: 'relative' }}>
                <div className="flex-fill">
                    <TitleSection
                        setShowFilter={setShowFilter}
                        projectTitleClicked={projectTitleClicked}
                        project={project}
                        id={id}
                        slug={slug}
                    />
                    <FilterSection showFilter={showFilter} setShowFilter={setShowFilter} />

                    <div className="project-workspace-section bg-light ">
                        <Route
                            path={`/project/${id}/${slug}/make-query`}
                            component={() => <MakeQueryForm id={id} slug={slug} />}
                        />
                        <Route
                            path={`/project/${id}/${slug}/view-discussion`}
                            component={() => (
                                <EverythingBelowNavbar
                                    project={project}
                                    id={id}
                                    slug={slug}
                                    fetchProjectDiscussion={fetchProjectDiscussion}
                                />
                            )}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default withRouter(Project);
