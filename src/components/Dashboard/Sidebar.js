import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import axios from 'axios';
import '../../styles/dashboardSidebar.css';

import { removeToken, getToken } from '../../helpers/authHelpers';

function Sidebar(props) {
    // methods
    const { displayLoader, toggleSidebarDashboard, setSidebarCurrentItem } = props;
    // states
    const { showSidebar, sidebarCurrentItem } = props;

    const handleLogout = () => {
        console.log(getToken());
        displayLoader(true);
        axios
            .put(
                'http://localhost:8000/api/users/logout',
                {},
                {
                    headers: {
                        Authorization: getToken(),
                    },
                }
            )
            .then((res) => {
                console.log(res);
                displayLoader(null);
                if (res.data.success) {
                    removeToken();
                    props.history.push('/');
                }
            })
            .catch((err) => {
                console.log(err);
                console.log(err.response);
                displayLoader(false);
            });
    };

    const toggleSidebar = async () => {
        toggleSidebarDashboard();
    };

    const viewProjects = () => {
        props.history.push('/dashboard/view-projects');
    };

    return (
        <div className={`dashboard-sidebar dashboard-sidebar-${showSidebar ? 'show' : 'hide'} col-sm-3 col-lg-2`}>
            <div className="d-flex">
                <h5 className="p-1 my-3 ">
                    <span className="dashboard-sidebar-username">Username</span>
                    <span className="dashboard-sidebar-email">some@email.com</span>
                </h5>

                <i className="fas fa-arrow-left dashboard-sidebar-arrow" onClick={() => toggleSidebar()}></i>
            </div>
            <hr />
            <Link to="/dashboard/view-projects" className="sidebar-link text-decoration-none">
                <div
                    className={`small dashboard-sidebar-btn ${
                        sidebarCurrentItem === 'view_projects' ? 'active' : null
                    } p-2`}
                    onClick={() => {
                        viewProjects();
                        setSidebarCurrentItem('view_projects');
                    }}
                >
                    {' '}
                    <i className="fas fa-folder mr-1"></i> <span>view projects</span>
                </div>
            </Link>
            <Link to="/dashboard/change-username" className="sidebar-link text-decoration-none">
                <div
                    className={`small dashboard-sidebar-btn ${
                        sidebarCurrentItem === 'change_username' ? 'active' : null
                    }  p-2`}
                    onClick={() => {
                        setSidebarCurrentItem('change_username');
                    }}
                >
                    {' '}
                    <i className="fas fa-user-alt mr-1"></i> <span>change username</span>
                </div>
            </Link>
            <Link to="/dashboard/change-email" className="sidebar-link text-decoration-none">
                <div
                    className={`small dashboard-sidebar-btn ${
                        sidebarCurrentItem === 'change_email' ? 'active' : null
                    }  p-2`}
                    onClick={() => {
                        setSidebarCurrentItem('change_email');
                    }}
                >
                    {' '}
                    <i className="fas fa-envelope mr-2"></i>
                    <span className="">change email</span>
                </div>
            </Link>
            <Link to="/dashboard/change-password" className="sidebar-link text-decoration-none">
                <div
                    className={`small dashboard-sidebar-btn ${
                        sidebarCurrentItem === 'change_password' ? 'active' : null
                    }  p-2`}
                    onClick={() => {
                        setSidebarCurrentItem('change_password');
                    }}
                >
                    {' '}
                    <i className="fas fa-key mr-1"></i> <span>change password</span>
                </div>
            </Link>
            <div
                className={`small dashboard-sidebar-btn p-2`}
                onClick={() => {
                    handleLogout();
                }}
            >
                {' '}
                <i className="fas fa-sign-out-alt mr-1"></i> <span>logout</span>
            </div>
            <div className="sidebar-brand">crossOver Nepal</div>
        </div>
    );
}

export default withRouter(Sidebar);
