import React from 'react';
import { withRouter } from 'react-router-dom';

function FilterContainer(props) {
    // methods
    const { toggleSidebarDashboard } = props;

    const showCreateProjectForm = () => {
        props.history.push('/dashboard/create-project');
    };

    return (
        <div className="dashboard-filter-container border-bottom p-3 shadow-sm d-flex">
            <div className="col-6">
                <div className="d-flex align-items-center">
                    <i
                        className="fas fa-bars  mr-3 dashboard-filterContainer-menu"
                        onClick={() => toggleSidebarDashboard()}
                    ></i>
                    <input type="search" placeholder="search project" className="form-control rounded-0" />
                </div>
            </div>
            <div className="col-6">
                <button className="btn btn-primary rounded-0" onClick={() => showCreateProjectForm()}>
                    <i className="fas fa-folder-plus mr-2"></i>
                    <span className="">create project</span>
                </button>
            </div>
        </div>
    );
}

export default withRouter(FilterContainer);
