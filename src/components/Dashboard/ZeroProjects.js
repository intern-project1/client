import React from 'react';
import { Link } from 'react-router-dom';

import '../../styles/projectsContainer.css';

function ZeroProjects() {
    return (
        <div className="zero-projects-container">
            <h5>No projects to show</h5>
            <Link to="/dashboard/create-project">create project</Link>
        </div>
    );
}

export default ZeroProjects;
