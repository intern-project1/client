import React from 'react';
import { Link } from 'react-router-dom';

import ZeroProjects from './ZeroProjects';
import '../../styles/projectsContainer.css';

function ProjectsContainer(props) {
    //    states
    const { projects } = props;

    return (
        <>
            <div className="dashboard-projects-container d-flex p-3">
                {!projects.length && <ZeroProjects />}
                <div className="col-12 col-lg-8">
                    {!!projects.length &&
                        projects.map((project) => {
                            return (
                                <Link
                                    key={project.id}
                                    to={'/project/' + project.id + '/' + project.project_slug}
                                    className="text-decoration-none text-muted"
                                >
                                    <div className="dashboard-project p-4 mr-2 my-3 border rounded ">
                                        <h4 className="projectContainer-projectTitle">{project.project_name}</h4>
                                        <span className=" projectContainer-project-description">
                                            <span>{project.project_description.substr(0, 120) + '...'}</span>

                                            <span className="text-primary project-seemore-text">see more</span>
                                        </span>
                                    </div>
                                </Link>
                            );
                        })}
                </div>
                <div className="p-5 bg-white rounded border col-0 col-lg-4 my-3 d-none d-lg-block ">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos tenetur amet quidem maxime asperiores
                    culpa dolorum delectus nulla earum sunt?
                </div>
            </div>
        </>
    );
}

export default ProjectsContainer;
