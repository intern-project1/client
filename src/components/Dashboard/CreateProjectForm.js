import React, { useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import axios from 'axios';
import { Link } from 'react-router-dom';

import LoadingButton from '../../utils/LoadingButton';
import { getToken } from '../../helpers/authHelpers';

function CreateProjectForm(props) {
    // methods
    const { setNewProjectCreatedState } = props;

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    const [submitLoading, setSubmitLoading] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('project_name', title);
        formData.append('project_description', description);
        formData.append('project_slug', title.replaceAll(' ', '-'));
        console.log(getToken());
        setSubmitLoading(true);
        axios
            .post('http://localhost:8000/api/projects/create', formData, {
                headers: {
                    Authorization: getToken(),
                },
            })
            .then((res) => {
                console.log(res);
                setSubmitLoading(null);
                if (res.data.success) {
                    setSuccessMsg('project created successfully');
                    setErrMsg(null);
                    setNewProjectCreatedState((prev) => !prev);
                }
            })
            .catch((err) => {
                console.log(err);
                console.log(err.response);
                setSubmitLoading(null);
                setErrMsg('sorry, something went wrong');
                setSuccessMsg(null);
            });
    };

    return (
        <div className="">
            <form
                className="dashboard-create-project-form bg-white rounded border shadow-sm p-4"
                onSubmit={(e) => handleSubmit(e)}
            >
                <h4 className="my-3">Create Project</h4>
                {errMsg && <div className="alert alert-danger small ">{errMsg}</div>}
                {successMsg && <div className="alert alert-success small ">{successMsg}</div>}
                <div className="my-3 ">
                    <label htmlFor="project-name" className="m-0">
                        Project title
                    </label>
                    <input
                        type="text"
                        className="form-control bg-light"
                        id="project-name"
                        spellCheck={true}
                        value={title}
                        onChange={(e) => {
                            setTitle(e.target.value);
                            setErrMsg(null);
                            setSuccessMsg(null);
                        }}
                    />
                </div>
                <div className="my-3 ">
                    <label htmlFor="project-description" className="m-0">
                        short description
                    </label>

                    <TextareaAutosize
                        id="project-description"
                        className="form-control bg-light"
                        minRows={2}
                        maxRows={10}
                        value={description}
                        onChange={(e) => {
                            setDescription(e.target.value);
                            setErrMsg(null);
                            setSuccessMsg(null);
                        }}
                    />
                </div>
                <div className="my-2">
                    {!submitLoading && (
                        <>
                            <button type="submit" className="btn btn-primary rounded-0">
                                Create
                            </button>
                            <Link to="/dashboard/view-projects" className="btn btn-secondary rounded-0 ml-2">
                                <i className="fas fa-times mr-1"></i>
                                <span>exit</span>
                            </Link>
                        </>
                    )}
                    {submitLoading && <LoadingButton className="btn btn-primary" />}
                </div>
            </form>
        </div>
    );
}

export default CreateProjectForm;
