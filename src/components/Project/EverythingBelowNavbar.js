import React from 'react';
import { Link } from 'react-router-dom';

import ProjectSidebar from './ProjectSidebar';
import Query from './Query';

function EverythingBelowNavbar(props) {
    // states
    const { project, id, slug } = props;
    // methods
    const { fetchProjectDiscussion } = props;

    return (
        <>
            <div className="d-flex">
                <ProjectSidebar />
                <div className="discussion-container flex-fill text-dark bg-white border rounded p-4">
                    {project &&
                        project.queries.map((query) => {
                            return (
                                <Query key={query.id} query={query} fetchProjectDiscussion={fetchProjectDiscussion} />
                            );
                        })}
                    {project && !project.queries.length && (
                        <div
                            className="text-center"
                            style={{
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(0%,-50%)',
                            }}
                        >
                            <h5>No queries created yet</h5>
                            <Link to={`/project/${id}/${slug}/make-query`}>make a query</Link>
                        </div>
                    )}
                </div>
            </div>
        </>
    );
}

export default EverythingBelowNavbar;
