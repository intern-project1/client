import React from 'react';
import { Link } from 'react-router-dom';

function ProjectSidebar() {
    return (
        <>
            <div className="project-sidebar bg-white mr-3 p-3 border rounded">
                <Link to="/dashboard/view-projects" className="btn btn-block btn-success rounded-0">
                    View projects
                </Link>
            </div>
        </>
    );
}

export default ProjectSidebar;
