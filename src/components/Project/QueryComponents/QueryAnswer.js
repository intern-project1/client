import React from 'react';
import moment from 'moment';

function QueryAnswer(props) {
    // states
    const { answer } = props;

    return (
        <>
            <section className="query-answers-section-child my-3">
                <div className="query-answers-section-border"></div>
                <div className="ml-3">
                    <h6 className="text-primary m-0 d-inline">{answer.user.username}</h6>
                    <small className="text-muted ml-2">- {moment(answer.created_at).format('YYYY-MM-DD')}</small>
                    <div>{answer.answer}</div>
                </div>
            </section>
        </>
    );
}

export default QueryAnswer;
