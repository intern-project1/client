import React from 'react';
import moment from 'moment';

function QueryDetails(props) {
    // states
    const { query } = props;

    return (
        <>
            <div className="project-details-container my-2 bg-light p-3 text-muted rounded">
                <div className="my-2">
                    <strong className="d-block">title</strong>
                    <span>{query.title}</span>
                </div>
                <div className="my-2">
                    <strong className="d-block">menu</strong>
                    <span>{query.menu}</span>
                </div>
                <div className="my-2">
                    <strong className="d-block">sub menu</strong>
                    <span>{query.sub_menu}</span>
                </div>
                <div className="my-2">
                    <strong className="d-block">content</strong>
                    <span>{query.content}</span>
                </div>
                <div className="my-2">
                    <strong className="d-block">reference</strong>
                    <span>{query.reference}</span>
                </div>
                <div className="my-2">
                    <strong className="d-block">created by</strong>
                    <span>{query.user.username}</span>
                </div>
                <div className="my-2">
                    <strong className="d-block">created at</strong>
                    <span>
                        {moment(query.created_at).fromNow() + ', ' + moment(query.created_at).format('YYYY-MM-DD')}
                    </span>
                </div>
            </div>
        </>
    );
}

export default QueryDetails;
