import Axios from 'axios';
import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import TextareaAutosize from 'react-textarea-autosize';

import { getToken } from '../../helpers/authHelpers';
import LoadingButton from '../../utils/LoadingButton';
import '../../styles/makeQueryForm.css';

function MakeQueryForm(props) {
    const { id, slug } = props;

    const [menu, setMenu] = useState('');
    const [subMenu, setSubMenu] = useState('');
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [reference, setReference] = useState('');

    const [successMsg, setSuccessMsg] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [submitLoader, setSubmitLoader] = useState(null);

    const cancelClicked = () => {
        props.history.push(`/project/${id}/${slug}/view-discussion`);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('menu', menu);
        formData.append('sub_menu', subMenu);
        formData.append('title', title);
        formData.append('content', content);
        formData.append('reference', reference);
        formData.append('project_id', id);

        setSubmitLoader(true);
        Axios.post('http://localhost:8000/api/queries/create', formData, {
            headers: {
                Authorization: getToken(),
            },
        })
            .then((res) => {
                console.log(res);
                setSubmitLoader(null);
                if (res.data.success) {
                    setSuccessMsg('query has been created');
                    setErrMsg(null);
                }
            })
            .catch((err) => {
                console.log(err);
                console.log(err.response);
                setSubmitLoader(null);
                setErrMsg('sorry something went wrong');
                setSuccessMsg(null);
            });
    };

    return (
        <div>
            <form className="mx-auto p-4 make-query-form" onSubmit={(e) => handleSubmit(e)}>
                <h4 className="text-dark mb-4">Make Query</h4>
                {successMsg && <div className="alert alert-success small">{successMsg}</div>}
                {errMsg && <div className="alert alert-danger small">{errMsg}</div>}
                <div className="my-2">
                    <label htmlFor="menu" className="m-0">
                        Menu
                    </label>
                    <input
                        type="text"
                        id="menu"
                        className="form-control"
                        value={menu}
                        onChange={(e) => {
                            setMenu(e.target.value);
                            setSuccessMsg(null);
                            setErrMsg(null);
                        }}
                    />
                </div>
                <div className="my-2">
                    <label htmlFor="sub-menu" className="m-0">
                        Sub menu
                    </label>
                    <input
                        type="text"
                        id="sub-menu"
                        className="form-control"
                        onChange={(e) => {
                            setSubMenu(e.target.value);
                            setSuccessMsg(null);
                            setErrMsg(null);
                        }}
                    />
                </div>
                <div className="my-2">
                    <label htmlFor="title" className="m-0">
                        Title
                    </label>
                    <input
                        type="text"
                        id="title"
                        className="form-control"
                        value={title}
                        onChange={(e) => {
                            setTitle(e.target.value);
                            setSuccessMsg(null);
                            setErrMsg(null);
                        }}
                    />
                </div>
                <div className="my-2">
                    <label htmlFor="content" className="m-0">
                        Content
                    </label>
                    <TextareaAutosize
                        id="content"
                        className="form-control"
                        minRows={2}
                        maxRows={10}
                        value={content}
                        onChange={(e) => {
                            setContent(e.target.value);
                            setSuccessMsg(null);
                            setErrMsg(null);
                        }}
                    />
                </div>
                <div className="my-2">
                    <label htmlFor="reference" className="m-0">
                        Reference
                    </label>
                    <TextareaAutosize
                        id="reference"
                        className="form-control"
                        minRows={2}
                        maxRows={10}
                        value={reference}
                        onChange={(e) => {
                            setReference(e.target.value);
                            setSuccessMsg(null);
                            setErrMsg(null);
                        }}
                    />
                </div>
                <div className="my-3">
                    {!submitLoader && (
                        <>
                            <button type="submit" className="btn btn-success rounded-0">
                                submit
                            </button>
                            <button
                                type="button"
                                className="btn btn-secondary rounded-0 ml-2"
                                onClick={() => cancelClicked()}
                            >
                                <i className="fas fa-times mr-1"></i>
                                <span>exit</span>
                            </button>
                        </>
                    )}
                    {submitLoader && <LoadingButton className="btn btn-success rounded-0" />}
                </div>
            </form>
        </div>
    );
}

export default withRouter(MakeQueryForm);
