import React, { useState } from 'react';
import TextAreaAutoSize from 'react-textarea-autosize';
import moment from 'moment';
import Axios from 'axios';

import QueryAnswer from './QueryComponents/QueryAnswer';
import QueryDetails from './QueryComponents/QueryDetails';
import '../../styles/query.css';
import LoadingButton from '../../utils/LoadingButton';
import { getToken } from '../../helpers/authHelpers';

function Query(props) {
    // states
    const { query } = props;
    // methods
    const { fetchProjectDiscussion } = props;

    const [viewDetails, setViewDetails] = useState(null);
    const [viewDiscussion, setViewDiscussion] = useState(null);

    const [reply, setReply] = useState('');
    const [replySubmitLoader, setReplySubmitLoader] = useState(null);

    const toggleViewDetails = () => {
        setViewDetails((prev) => !prev);
    };

    const toggleViewDiscussion = () => {
        setViewDiscussion((prev) => !prev);
    };

    const handleReplySubmit = () => {
        const formData = new FormData();
        formData.append('answer', reply);

        setReplySubmitLoader(true);
        Axios.post('http://localhost:8000/api/answers/create/' + query.id, formData, {
            headers: {
                Authorization: getToken(),
            },
        })
            .then((res) => {
                console.log(res);
                setReplySubmitLoader(null);
                if (res.data.success) {
                    setReply('');
                    fetchProjectDiscussion();
                }
            })
            .catch((err) => {
                console.log(err);
                console.log(err.response);
                setReplySubmitLoader(null);
            });
    };

    return (
        <>
            <div className="">
                <main className="query-section-parent d-flex align-items-center justify-content-between">
                    <div>
                        <h5 className="m-0">{query.title}</h5>
                        <span className="text-muted small">{moment(query.created_at).format('YYYY-MM-DD') + ', '}</span>
                        <span className="text-primary small">{query.user.username}</span>
                    </div>
                    <div className="project-view-details-btn text-primary">
                        {!viewDetails ? (
                            <span onClick={() => toggleViewDetails()}>view details</span>
                        ) : (
                            <span onClick={() => toggleViewDetails()}>hide details</span>
                        )}
                    </div>
                </main>
                {!!viewDetails && <QueryDetails query={query} />}
                <div className="my-3">
                    <strong
                        className="text-muted view-hide-discussion-btn"
                        onClick={() => {
                            toggleViewDiscussion();
                        }}
                    >
                        {!viewDiscussion ? (
                            <span>view discussions ({query.answers.length})</span>
                        ) : (
                            <span>hide discussions ({query.answers.length})</span>
                        )}
                    </strong>
                </div>
                {!!viewDiscussion &&
                    !!query.answers.length &&
                    query.answers.map((answer) => <QueryAnswer key={answer.id} answer={answer} />)}
                <div className="d-flex">
                    <TextAreaAutoSize
                        type="text"
                        className="form-control bg-light query-make-reply-input"
                        placeholder="write a reply"
                        minRows={1}
                        maxRows={10}
                        value={reply}
                        onChange={(e) => {
                            setReply(e.target.value);
                        }}
                    />
                    {!replySubmitLoader && (
                        <button
                            className="btn btn-sm btn-light border text-muted mx-2 query-reply-submit-btn"
                            onClick={() => handleReplySubmit()}
                        >
                            <i className="fas fa-check"></i>
                        </button>
                    )}
                    {replySubmitLoader && <LoadingButton className="btn btn-light border text-muted mx-2" />}
                </div>
            </div>
            <hr style={{ borderColor: '#ddd', margin: '30px 0px' }} />
        </>
    );
}

export default Query;
