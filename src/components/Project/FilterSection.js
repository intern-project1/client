import React from 'react';

function FilterSection(props) {
    // states
    const { showFilter } = props;
    // methods
    const { setShowFilter } = props;

    return (
        <div
            className={`project-filter-section ${
                showFilter ? 'show' : 'hide'
            } p-2 shadow-sm text-center d-flex align-items-center`}
        >
            <div className="flex-fill">
                <h1>filter section</h1>
            </div>
            <div className="project-filter-section-close">
                <i className="btn btn-light text-primary far fa-times-circle" onClick={() => setShowFilter(null)}></i>
            </div>
        </div>
    );
}

export default FilterSection;
