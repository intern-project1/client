import React from 'react';
import { Link } from 'react-router-dom';

function TitleSection(props) {
    // states
    const { id, slug, project } = props;
    // methods
    const { projectTitleClicked, setShowFilter } = props;

    return (
        <div className="project-title-section p-2 shadow-sm text-center d-flex align-items-center justify-content-around">
            <div className="ml-2 mr-4 project-title-div" onClick={() => projectTitleClicked()}>
                <h4 className="my-0 text-light">{project && project.project_name}</h4>
                <span className="small text-light">dec 07, 2020</span>
            </div>
            <div>
                <button className="btn btn-info rounded-0" onClick={() => setShowFilter((prev) => !prev)}>
                    <i className="fas fa-filter mr-1"></i>
                    <span>filter</span>
                </button>
                <Link to={`/project/${id}/${slug}/make-query`} className="btn btn-danger rounded-0 ml-2">
                    <span>make query</span>
                </Link>
            </div>
        </div>
    );
}

export default TitleSection;
