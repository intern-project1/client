import React from 'react';
import '../../styles/dashboardModal.css';
import ChangeEmail from './ChangeEmail';
import ChangePassword from './ChangePassword';
import ChangeUsername from './ChangeUsername';

function DashboardModal(props) {
    // states
    const { showEmailModal, showUsernameModal, showPasswordModal } = props;
    // methods
    const { closeAllModals } = props;
    return (
        <>
            {showEmailModal && <ChangeEmail showEmailModal={showEmailModal} closeAllModals={closeAllModals} />}
            {showUsernameModal && (
                <ChangeUsername showUsernameModal={showUsernameModal} closeAllModals={closeAllModals} />
            )}
            {showPasswordModal && (
                <ChangePassword showPasswordModal={showPasswordModal} closeAllModals={closeAllModals} />
            )}
        </>
    );
}

export default DashboardModal;
