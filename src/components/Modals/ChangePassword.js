import React from 'react';

function ChangePassword(props) {
    return (
        // <div className="dashboardModal-container shadow-lg border rounded p-4">
        <form className="dashboard-change-email-form border p-4 text-muted bg-white shadow-sm">
            <div className="d-flex align-items-center">
                <span className="text-center flex-fill dashboardModal-title">Change password</span>
            </div>

            <div className="my-2">
                <label htmlFor="email" className="m-0 small">
                    <i className="fas fa-envelope mr-2"></i>
                    <span className="">type email</span>
                </label>
                <input type="email" id="email" className="form-control" />
            </div>

            <div className="my-3">
                <button className="btn dashboardModal-btn" disabled={true}>
                    Feature not available
                </button>
            </div>
        </form>
        // </div>
    );
}

export default ChangePassword;
