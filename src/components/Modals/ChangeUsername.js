import React from 'react';

function ChangeUsername(props) {
    return (
        // <div className="dashboardModal-container shadow-lg border rounded p-4">
        <form className="dashboard-change-email-form border p-4 text-muted bg-white shadow-sm">
            <div className="d-flex align-items-center">
                <span className="text-center flex-fill dashboardModal-title">Change username</span>
            </div>
            <div className="my-2">
                <label htmlFor="username" className="m-0 small">
                    <i className="fas fa-user-alt mr-1"></i> <span>new username</span>
                </label>
                <input type="text" id="username" className="form-control" />
            </div>

            <div className="my-2">
                <label htmlFor="password" className="m-0 small">
                    <i className="fas fa-key mr-1"></i> <span>type password</span>
                </label>
                <input type="password" id="password" className="form-control" />
            </div>
            <div className="my-3">
                <button className="btn dashboardModal-btn" disabled={true}>
                    Feature not available
                </button>
            </div>
        </form>
        // </div>
    );
}

export default ChangeUsername;
