import React from 'react';

function AdminSidebar(props) {
    return (
        <div className="admin-sidebar col-sm-3 col-lg-2">
            <div className="d-flex">
                <h5 className="p-1 my-3 ">
                    <span className="admin-sidebar-username">Username</span>
                    <span className="admin-sidebar-email">some@email.com</span>
                </h5>

                <i className="fas fa-arrow-left admin-sidebar-arrow"></i>
            </div>
            <hr />
            <div className="small admin-sidebar-btn p-2">
                {' '}
                <i className="fas fa-user-edit mr-1"></i> <span>create user</span>
            </div>
            <div className="small admin-sidebar-btn p-2">
                {' '}
                <i className="fas fa-users mr-2"></i>
                <span className="">view users</span>
            </div>

            <div className="sidebar-brand">crossOver Nepal</div>
        </div>
    );
}

export default AdminSidebar;
