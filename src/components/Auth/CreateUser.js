import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import Axios from 'axios';
import '../../styles/authForms.css';

import { saveToken } from '../../helpers/authHelpers';
import LoadingButton from '../../utils/LoadingButton';

function CreateUser(props) {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [token, setToken] = useState(null);

    const [errMsg, setErrMsg] = useState(null);
    const [success, setSuccess] = useState(null);
    const [submitLoading, setSubmitLoading] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('username', username);
        formData.append('email', email);
        formData.append('password', password);
        if (!username.length || !email.length || !password.length) {
            setErrMsg('all fields are required');
            return;
        }

        setSubmitLoading(true);
        Axios.post('http://127.0.0.1:8000/api/users/signup', {
            username,
            email,
            password,
        })
            .then((res) => {
                // console.log(res);
                setSubmitLoading(null);
                if (res.data.success) {
                    setSuccess(true);
                    setToken(res.data.token);
                }
                if (!res.data.success) {
                    setErrMsg(res.data.message);
                }
            })
            .catch((err) => {
                setSubmitLoading(false);
                console.log(err.response);
                if (err.response) {
                    setErrMsg(err.response.data.message);
                }
            });
    };

    const handleEnterDashboard = () => {
        const { success } = saveToken(token);
        if (success) {
            props.history.push('/dashboard/view-projects');
        } else {
            setErrMsg('unable to save token');
        }
    };

    return (
        <>
            <form onSubmit={(e) => handleSubmit(e)}>
                {errMsg && <div className="alert alert-danger small my-4">{errMsg}</div>}
                {success && <div className="alert alert-success small my-4">{'member created successfully'}</div>}
                <div className="my-4">
                    <label htmlFor="login-username" className="mb-1 small">
                        <i className="fas fa-user-alt mr-1"></i> <span>Username</span>
                    </label>
                    <input
                        type="text"
                        id="login-username"
                        className="form-control"
                        value={username}
                        onChange={(e) => {
                            setUsername(e.target.value);
                            setErrMsg(null);
                            setSuccess(null);
                        }}
                    />
                </div>
                <div className="my-4">
                    <label htmlFor="login-email" className="mb-1 small">
                        <i className="fas fa-envelope mr-2"></i>
                        <span className="">Email</span>
                    </label>
                    <input
                        type="email"
                        id="login-email"
                        className="form-control"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                            setErrMsg(null);
                            setSuccess(null);
                        }}
                    />
                </div>
                <div className="my-4">
                    <label htmlFor="login-password" className="mb-1 small">
                        <i className="fas fa-key mr-1"></i> <span>Password</span>
                    </label>
                    <input
                        type="password"
                        id="login-password"
                        className="form-control"
                        value={password}
                        onChange={(e) => {
                            setPassword(e.target.value);
                            setErrMsg(null);
                        }}
                    />
                </div>
                <div>
                    {!submitLoading && (
                        <button type="submit" className="btn create-button ">
                            Create
                        </button>
                    )}
                    {submitLoading && <LoadingButton className={'btn create-button'} />}
                    {success && (
                        <button
                            type="button"
                            className="btn btn-primary ml-3 enter-dashboard-button"
                            onClick={() => handleEnterDashboard()}
                        >
                            enter dashboard
                        </button>
                    )}
                </div>
            </form>
        </>
    );
}

export default withRouter(CreateUser);
