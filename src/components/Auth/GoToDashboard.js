import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import '../../styles/authForms.css';

import { saveToken } from '../../helpers/authHelpers';
import LoadingButton from '../../utils/LoadingButton';

function GoToDashboard(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [errMsg, setErrMsg] = useState(null);
    const [submitLoading, setSubmitLoading] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);

        setSubmitLoading(true);
        axios
            .post('http://localhost:8000/api/users/login', formData)
            .then((res) => {
                console.log(res);
                setSubmitLoading(null);
                if (res.data.success) {
                    const { success } = saveToken(res.data.token);
                    if (success) {
                        props.history.push('/dashboard/view-projects');
                    } else {
                        setErrMsg('unable to save token');
                    }
                }
            })
            .catch((err) => {
                console.log(err);
                console.log(err.response);
                if (err.response.data) {
                    setErrMsg(err.response.data.message);
                }
                setSubmitLoading(null);
            });
    };

    return (
        <>
            <form onSubmit={(e) => handleSubmit(e)}>
                {errMsg && <div className="alert alert-danger small my-4">{errMsg}</div>}
                <div className="my-4">
                    <label htmlFor="login-email" className="mb-1 small">
                        <i className="fas fa-envelope mr-2"></i> <span>Email</span>
                    </label>
                    <input
                        type="email"
                        id="login-email"
                        className="form-control"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                        }}
                    />
                </div>
                <div className="my-4">
                    <label htmlFor="login-password" className="mb-1 small">
                        <i className="fas fa-key mr-1"></i> <span>Password</span>
                    </label>
                    <input
                        type="password"
                        className="form-control"
                        value={password}
                        onChange={(e) => {
                            setPassword(e.target.value);
                        }}
                    />
                </div>
                <div>
                    {!submitLoading && (
                        <button type="submit" className="btn goToDashboard-button">
                            Enter
                        </button>
                    )}
                    {submitLoading && <LoadingButton className={'btn create-button'} />}
                </div>
            </form>
        </>
    );
}

export default withRouter(GoToDashboard);
